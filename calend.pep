; ********************************************************* 
; Programme: calend.txt version PEP813 sous Windows
;
; Le but de ce programme est de demander une date au format AAAAMM et afficher l'ann�e et le mois en texte ainsi qu'une repr�sentation
; graphique du mois soud la forme d'une page de calendrier. Le programme affiche ensuite les jours de paies et recommence jusqu'� ce que la
; valeur 9999 soit entr�e.
;
;
;       auteur:   Olivier Leduc
;       courriel: leduc.olivier.2@courrier.uqam.ca
;       code permanent: LEDO10069200
;       date:     hiver 2016
;       cours:    INF2170-11
; *********************************************************
; ===============================================================================================================================================================
;                                                                           --- MAIN ---
; ===============================================================================================================================================================
         STRO    msgintro,d
debut:   STRO    msgquest,d
         LDA     0,i
         LDX     0,i
         STA     annee,d     ; Initalisation des variables
         STA     mois,d
         STA     curseur,d 
         STA     jourspai,x
         LDX     2,i
         STA     jourspai,x
         LDX     4,i
         STA     jourspai,x

FOR01:   LDX     0,i         ; Initialisation du buffer
         LDA     "--",i
FTEST01: CPX     254,i
         BRGE    ENDFOR01,i
         STA     tampon,x
         ADDX    2,i
         BR      FTEST01,i 
ENDFOR01:NOP0    
         
         LDX     0,i         ; => Initialisation de la boucle de lecture de l'entr�e
         LDA     0,i
boucle1: CHARI   chaine,x    ; Lecture de l'entr�e
         LDBYTEA chaine,x
         ADDX    1,i
         CPA     10,i        ; Si caract�re ENT -> Traitement de la chaine
         BREQ    lectured,i
         CPA     '0',i       ; Si caract�re n'est pas un chiffre -> Invalide
         BRLT    invalidc,i 
         CPA     '9',i       ; Si caract�re n'est pas un chiffre -> Invalide
         BRGT    invalidc,i
         CPX     7,i         ; Si compteur de chaine > 7 -> Invalide
         BRGT    invalidc,i
         BR      boucle1,i   ; Fin de la boucle1

lectured:LDA     4,i         ; => Lecture de l'ann�e
         STA     -4,s        ; - Empilage du param�re #nbcaract
         LDA     chaine,i    ; --
         STA     -6,s        ; - Empilage du param�tre #addrprot
         SUBSP   6,i         ; - Allocation de l'espace sur le stack #nbeni16 #nbcaract #addrprot
         CALL    AsciiI16,i  ; -
         LDA     0,s         ; - R�cup�ration du r�sultat
         STA     annee,d     ; -- Enregistrement de l'ann�e
         ADDSP   2,i         ; -- Nettoyage du stack -> #nbeni16
         CPA     9999,i
         BREQ    fin,i

         LDA     2,i         ; => Lecture du mois
         STA     -4,s        ; - Empilage du param�re #nbcaract
         LDA     chaine,i    ; -- Positionement au bon caract�re dans la chaine
         ADDA    4,i         ; --
         STA     -6,s        ; - Empilage du param�tre #addrprot
         SUBSP   6,i         ; - Allocation de l'espace sur le stack #nbeni16 #nbcaract #addrprot
         CALL    AsciiI16,i  ; -
         LDA     0,s         ; - R�cup�ration du r�sultat
         STA     mois,d      ; -- Enregistrement de l'ann�e
         ADDSP   2,i         ; -- Nettoyage du stack -> #nbeni16

validerl:LDA     annee,d     ; => Validation des valeurs l'entr�e
         CPA     1901,i      ; -
         BRLT    invalidc,i  ; -
         CPA     2050,i      ; -
         BRGT    invalidc,i  ; -
         LDA     mois,d      ; -
         CPA     1,i         ; -
         BRLT    invalidc,i  ; -
         CPA     12,i        ; -
         BRGT    invalidc,i  ; -

calcjour:NOP0                ; => Calculer le nombre de jours par rapport � 190001
         LDA     annee,d
         SUBA    1900,i      ; aaxx
         STA     jours,d
         
         SUBA    1,i         ; �tape 1: (aaxx - 1) / 4
         ASRA                ; - /2    
         ASRA                ; - /4
                             ; Accumulation du r�sultat de l'�tape 1
         ADDA    jours,d     ; - jours += (aaxx - 1) / 4
         STA     jours,d     ; --
         
         
         LDA     mois,d      ; �tape 2: (((month - 1) * 57) + 50) / 100
                             ; - Multiplication
         SUBA    1,i         ; -- Empilage de (mois - 1)
         STA     -4,s        ; --- facteur1 -> mois - 1
         LDA     57,i        ; -- Empilage de 57
         STA     -6,s        ; --- facteur2 -> 57
         SUBSP   6,i         ; -- Allocation de l'espace sur le stack #produit #facteur2 #facteur1
         CALL    multipli,i  ; -- 
         LDA     0,s         ; -- R�cup�ration du r�sultat -> (mois - 1) * 57
         ADDSP   2,i         ; --- Nettoyage du stack -> #produit
         ADDA    50,i        ; - Addition d'une constante: ((mois - 1) * 57) + 50
                             ; - Division
         STA     -8,s        ; -- Empilage de (((mois - 1) * 57) + 50)
         LDA     100,i       ; -- Empilage de 100
         STA     -6,s        ; -- 
         SUBSP   8,i         ; -- Allocation de l'espace sur le stack #reste #quotient #diviseur #dividend
         CALL    diviser,i   ; --
         LDA     0,s         ; -- R�cup�ration du quotient -> (((mois - 1) * 57) + 50) / 100
         ADDSP   4,i         ; --- Nettoyage du stack -> #quotient #reste
                             ; Accumulation du r�sultat de l'�tate 2
         ADDA    jours,d     ; - jours += (((month - 1) * 57) + 50) / 100;
         STA     jours,d     ; --
         
         LDA     mois,d      ; �tape 3: (month - 1) * 30
         SUBA    1,i         ; - Empilage de (mois - 1)
         STA     -4,s        ; -- facteur1 -> mois - 1
         LDA     30,i        ; - Empilage de 30
         STA     -6,s        ; -- facteur2 -> 30
         SUBSP   6,i         ; -- Allocation de l'espace sur le stack #produit #facteur2 #facteur1
         CALL    multipli,i  ; -- 
         LDA     0,s         ; -- R�cup�ration du r�sultat -> (mois - 1) * 30
         ADDSP   2,i         ; --- Nettoyage du stack -> #produit
                             ; Accumulation du r�sultat de l'�tate 3
         ADDA    jours,d     ; - jours += (((month - 1) * 57) + 50) / 100;
         STA     jours,d     ; --

         LDA     jours,d     ; �tape 4: jours + 1
         ADDA    1,i         ;-
         STA     jours,d     ;-

calcbisx:NOP0                ; => Calculer ann�es bisextiles
         LDA     0,i         ; - Initialiser bissextil
         STA     bisextil,d  ; --

         LDA     annee,d     ; - Est Bisextielle si (ann�e % 4 == 0)
         STA     -8,s        ; -- Division de l'ann�e par 4
         LDA     4,i         ; ---
         STA     -6,s        ; ---
         SUBSP   8,i         ; --- Allocation de l'espace sur le stack #reste #quotient #diviseur #dividend
         CALL    diviser,i   ; ---
         LDA     2,s         ; --- Chargement du r�sultat
         ADDSP   4,i         ; --- Nettoyage du stack -> #quotient #reste
         CPA     0,i         ; -- Si reste == 0
         BREQ    estbisex,i  ; -- Alors est bisextile

         LDA     annee,d     ; - Est Bisextielle si (ann�e % 400 == 0) et (ann�e % 100 == 0)
         STA     -8,s        ; -- Division de l'ann�e par 400
         LDA     400,i       ; ---
         STA     -6,s        ; ---
         SUBSP   8,i         ; --- Allocation de l'espace sur le stack #reste #quotient #diviseur #dividend
         CALL    diviser,i   ; ---
         LDA     2,s         ; --- Chargement du r�sultat
         ADDSP   4,i         ; --- Nettoyage du stack -> #quotient #reste
         CPA     0,i         ; -- Si reste != 0
         BRNE    pasbisex,i  ; -- Alors non bisextile
         LDA     annee,d     ; -
         STA     -8,s        ; -- Division de l'ann�e par 100
         LDA     100,i       ; ---
         STA     -6,s        ; ---
         SUBSP   8,i         ; --- Allocation de l'espace sur le stack #reste #quotient #diviseur #dividend
         CALL    diviser,i   ; ---
         LDA     2,s         ; --- Chargement du r�sultat
         ADDSP   4,i         ; --- Nettoyage du stack -> #quotient #reste
         CPA     0,i         ; -- Si reste != 0
         BRNE    pasbisex,i  ; -- Alors non bisextile
estbisex:LDA     1,i         ; -
         STA     bisextil,d  ; -

         LDA     mois,d      ; => Ajuster jours en fonction des ann�es bissextiles
         CPA     2,i         ; - Si mois > 2 && bisextile == 1
         BRLE    calcstrt,i  ; --
         LDA     jours,d     ; -- Alors on d�cr�mente jours de 1
         SUBA    1,i         ; ---
         STA     jours,d     ; ---
         BR      calcstrt,i  ; -
pasbisex:LDA     mois,d      ; -
         CPA     2,i         ; - Si mois > 2 && bisextile == 0
         BRLE    calcstrt,i  ; --
         LDA     jours,d     ; -- Alors on d�cr�mente jours de 2
         SUBA    2,i         ; ---
         STA     jours,d     ; ---

calcstrt:LDA     jours,d     ; => Calculer start_day: jours % 7 = jourdebu 
         STA     -8,s        ; -
         LDA     7,i         ; -
         STA     -6,s        ; -
         SUBSP   8,i         ; - Allocation de l'espace sur le stack #reste #quotient #diviseur #dividend
         CALL    diviser,i   ; -
         LDA     2,s         ; -
         ADDSP   4,i         ; - Nettoyage du stack -> #quotient #reste
         STA     jourdebu,d  ; - Sauvegarde du reste -> jourdebu

clcqtejs:LDA     0,i         ; => Calculer le nombre de jours dans le mois courrant
         LDX     mois,d      ; - Si mois == Fr�vrier
IF00:    CPX     2,i         ; --
         BREQ    THEN00,i    ; --
         BR      ENDIF00,i   ; --
THEN00:  LDA     bisextil,d  ; -- Et bisextil == 1
IF01:    CPA     1,i         ; ---
         BRNE    ENDIF00,i   ; --- 
THEN01:  LDA     1,i         ; --- Alors on rajoute un jours � A
ENDIF00: NOP0                ; - On r�cup�re la valeur dans un tableau
         SUBX    1,i         ; -- On enl�ve un � l'index car le tableau comment � z�ro
         ASLX                ; -- On multiplie le mois par deux car c'est un tableau de words
         ADDA    tbljours,x  ; -- On ajoute la valeur au registre A qui contient les ajustements
         STA     qtejmois,d  ; - On enregistre la valeur
         

calcjpai:NOP0                ; Calculer Pay Days
         LDA     jourdebu,d  ; - Si le premier jours de la semaine est < 4 alors (4-jourdebu)=jeudi et si >4 alors (11-jourdebu)=jeudi
         CPA     4,i         ; --
         BRLT    avanjeud,i  ; --
         BREQ    trvjpaie,i  ; --
         BRGT    aprsjeud,i  ; --
avanjeud:LDA     4,i         ; --
         BR      trvjpaie,i  ; --
aprsjeud:LDA     11,i        ; --
trvjpaie:SUBA    jourdebu,d  ; --
         SUBSP   4,i         ; - Allocation des variables temporares de calcul #vartmp02 #vartmp01 
         ADDA    jours,d     ; - Assigner la variable temporaire (jeudi courant en jours absolus)
         STA     vartmp01,s  ; --
         LDA     jours,d     ; - Assigner la variable temporaire (fin du mois en jours absolus)
         ADDA    qtejmois,d  ; --
         STA     vartmp02,s  ; --

         LDA     vartmp01,s  ; - Initialisation au premier jeudi du mois
         LDX     0,i         ; - Initialisation du registre X qui compte le nombre de jours de paie
WHILE01: NOP0                ; - Boucle pour tester tous les jeudis du mois
         CPA     vartmp02,s
         BRGE    ENDWHI01,i
         STA     -8,s
         LDA     14,i
         STA     -6,s
         SUBSP   8,i         ; - Allocation de l'espace sur le stack #reste #quotient #diviseur #dividend
         CALL    diviser,i
         LDA     2,s
         ADDSP   4,i         ; - Nettoyage du stack -> #quotient #reste
IF02:    CPA     4,i         ; - Si reste % 4
         BRNE    ENDIF02,i   ; --
THEN02:  LDA     vartmp01,s  ; -- Alors enregistrer jour le paie
         SUBA    jours,d     ; ---
         ADDA    1,i         ; ---
         ASLX                ; --- *2
         STA     jourspai,x  ; ---
         ASRX                ; --- /2
         ADDX    1,i         ; ---
ENDIF02: NOP0
         LDA     vartmp01,s  ; - Passer au jeudi suivant
         ADDA    7,i
         STA     vartmp01,s
         BR      WHILE01,i
ENDWHI01:NOP0                ; Tous les jeudis on �t� test�
         STX     nbjrspai,d  ; Enregistrer compteur de jours de paie
         ADDSP   4,i         ; Nettoyage du stack -> #vartmp02 #vartmp01


afficher:NOP0                ; =========> AFFICHAGE <========

         LDA     tampon,i    ; => Ajouter un saut de ligne
         STA     -4,s        ; - Empilage de l'adresse de tampon
         LDA     '\n',i      ; - Empilage du caract�re � ajouter
         STA     -2,s        ; --
         SUBSP   4,i         ; - Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; -
         
         LDA     tampon,i    ; => Ajouter un saut de ligne
         STA     -4,s        ; - Empilage de l'adresse de tampon
         LDA     '\n',i      ; - Empilage du caract�re � ajouter
         STA     -2,s        ; --
         SUBSP   4,i         ; - Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; -

         LDA     10,i        ; => Ajouter les 10 espaces initiaux
         STA     -2,s        ; -
         LDA     tampon,i    ; -
         STA     -4,s        ; -
         SUBSP   4,i         ; - #qteesp5 #addtpn5 
         CALL    AjtEsTmp,i  ; -

         NOP0                ; => Ajouter le mois dans le titre
         LDX     tampon,i    ; - Empilage du param�tre 1 (Adresse du tampon)
         STX     -8,s        ; --
         LDX     txtmois,i   ; - Empilage du param�tre 2 (Adresse de la chaine de copi�e)
         STX     -6,s        ; --
         NOP0                ; - Empilage du param�tre 3 (Position de d�but dans la chaine copi�e) 
         LDX     mois,d      ; -- On va chercher l'adresse de de la premi�re lettre du mois dans la table d'index
         SUBX    1,i         ; --- Index commence � z�ro donc (mois - 1) = index
         ASLX                ; --- *2 Car la c'est une table de WORDS
         LDX     tblmois,x   ; --- On va cherche la premi�re lettre du mois dans la table Ex: Avril (03) -> 21
         STX     -4,s        ; --3

         LDA     0,i         ; -
WHILE02: NOP0                ; - Empilage de la position de la fin de la copie
         LDBYTEA txtmois,x   ; -- On cherche le prochain d�limiteur
         CPA     '-',i       ; ---
         BREQ    ENDWHI02,i  ; ---
         ADDX    1,i         ; ---
         BR      WHILE02,i   ; ---
ENDWHI02:SUBX    -4,s        ; -- On soustrait la position de d�but
         STX     -2,s        ; --
         SUBSP   8,i         ; - Allocation de l'espace sur le stack #qtechrc6 #posdebu6 #addrorg6 #addrtpn6
         CALL    CpTampon,i  
         NOP0 
         
         LDA     1,i         ; => Ajouter les 1 espace entre le mois et l'ann�e
         STA     -2,s        ; -
         LDA     tampon,i    ; -
         STA     -4,s        ; -
         SUBSP   4,i         ; - #qteesp5 #addtpn5 
         CALL    AjtEsTmp,i  ; -


         LDA     tampon,i    ; => Affichage de l'ann�e
         STA     -4,s        ; -
         LDA     annee,d      ; - 
         STA     -2,s        ; -
         SUBSP   4,i         ; - #numero7 #addrtpn7 
         CALL    AjtNmTpn,i  ; -

         LDA     tampon,i    ; - Ajouter un saut de ligne
         STA     -4,s        ; -- Empilage de l'adresse de tampon
         LDA     '\n',i      ; -- Empilage du caract�re � ajouter
         STA     -2,s        ; ---
         SUBSP   4,i         ; -- Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; --
         LDA     tampon,i    ; - Ajouter un saut de ligne
         STA     -4,s        ; -- Empilage de l'adresse de tampon
         LDA     '\n',i      ; -- Empilage du caract�re � ajouter
         STA     -2,s        ; ---
         SUBSP   4,i         ; -- Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; --


         NOP0                ; => Affichage de l'index des jours
         LDX     tampon,i    ; -
         STX     -8,s        ; -
         LDX     txtidjrs,i  ; -
         STX     -6,s        ; -
         LDX     0,i         ; -
         STX     -4,s        ; -
         LDX     28,i        ; -
         STX     -2,s        ; -
         SUBSP   8,i         ; - Allocation de l'espace sur le stack #qtechrc6 #posdebu6 #addrorg6 #addrtpn6 
         CALL    CpTampon,i  ; -

         NOP0                ; => Affichage des jours du mois
         SUBSP   4,i         ; - Variables de calcul -> #vartmp02 #vartmp01 
         LDA     0,i         ; -
         STA     vartmp01,s  ; -
         STA     vartmp02,s  ; -

FOR02:   LDX     1,i         ; - Pour tous les jours de 1 � la quantit� de jours dans le mois
FTEST02: NOP0                ; --
         CPX     qtejmois,d  ; --
         BRGT    ENDFOR02,i  ; --
         ; # Boucle Jours ##########

IF03:    NOP0                ; -- Si le compteur de rang�e est �gal � z�ro on rajoute six espaces
         LDA     vartmp01,s  ; ---
         CPA     0,i         ; ---
         BRNE    ENDIF03,i   ; ---
THEN03:  NOP0                ; ---
         LDA     7,i         ; --- Empiler le nombre d'espaces
         STA     -2,s        ; ---
         LDA     tampon,i    ; --- Empiler l'addresse du tampon
         STA     -4,s        ; ---
         SUBSP   4,i         ; --- #qteesp5 #addtpn5 
         CALL    AjtEsTmp,i  ; ---
ENDIF03: NOP0                ; ---

IF04:    CPX     1,i         ; -- Ajout des espaces pour le premier jour du mois
         BRNE    ENDIF04,i   ; --
THEN04:  NOP0                ; --
         LDA     3,i         ; -- Multiplication du nombre d'espaces par le jour de d�part
         STA     -6,s        ; ---
         LDA     jourdebu,d  ; ---
         STA     -4,s        ; ---
         SUBSP   6,i         ; --- Allocation de l'espace sur le stack #produit #facteur2 #facteur1
         CALL    multipli,i  ; ---
         LDA     0,s         ; --- R�cup�ration du r�sultat
         ADDSP   2,i         ; --- Nettoyage du stack -> #produit 
         STA     -2,s        ; -- Empilage du nombre d'espace � ajouter
         LDA     tampon,i    ; -- Empilage de l'addresse du tampon
         STA     -4,s        ; ---
         SUBSP   4,i         ; -- #qteesp5 #addtpn5 
         CALL    AjtEsTmp,i  ; --
         LDA     jourdebu,d  ; -- On avance le compteur de rang�e car on passe ces jours dans la rang�e
         STA     0,s         ; ---
ENDIF04: NOP0                ; ---

IF05:    NOP0                ; -- Si le jours est > 9 alors on l'affiche une case avant
         CPX     10,i        ; ---
         BRLT    ELSE05,i    ; ---
THEN05:  NOP0                ; -- Alors on affiche le jour � la position curseur - 1
         LDA     curseur,d   ; --- D�cr�mentation du curseur 
         SUBA    1,i         ; ----
         STA     curseur,d   ; ----
         LDA     tampon,i    ; --- Empilage de l'adresse du tampon 
         STA     -4,s        ; ----
         STX     -2,s        ; --- Empilage du jour
         SUBSP   4,i         ; - #numero7 #addrtpn7 
         CALL    AjtNmTpn,i  ; ---
         BR      ENDIF05,i   ; ---
ELSE05:  NOP0                ; -- Sinon on l'affiche � curseur
         LDA     tampon,i    ; --- Empilage de l'adresse du tampon 
         STA     -4,s        ; ----
         STX     -2,s        ; --- Empilage du jour
         SUBSP   4,i         ; --- #numero7 #addrtpn7
         CALL    AjtNmTpn,i  ; ---
ENDIF05: NOP0                ; ---

         LDA     vartmp01,s  ; -- Incr�mentation du compteur de rang�e
         ADDA    1,i         ; --
         STA     vartmp01,s  ; --

IF06:    NOP0                ; -- Si le compteur de rang�e % 7 == 0
         LDA     vartmp01,s  ; --- Chargement redondant au cas ou on modifie le code plus tard
         STA     -8,s        ; --- Empilage du compteur de rang�e
         LDA     7,i         ; --- Empilage de 7
         STA     -6,s        ; --- 
         SUBSP   8,i         ; --- Allocation de l'espace sur le stack #reste #quotient #diviseur #dividend
         CALL    diviser,i   ; ---
         LDA     2,s         ; --- R�cup�ration du reste
         ADDSP   4,i         ; ---- Nettoyage du stack -> #quotient #reste
         CPA     0,i
         BRNE    ELSE06,i
THEN06:  NOP0                ; -- Alors on affiche un saut de ligne et on mets le compteur de rang�e � z�ro
         LDA     0,i         ; --- Mettre de compteur de rang�e � 0
         STA     vartmp01,s  ; ----
         CPX     qtejmois,d  ; --- Si c'est le derni� jours, pas besoin de saut de ligne
         BREQ    ENDIF06,i
         LDA     tampon,i    ; --- Ajouter un saut de ligne
         STA     -4,s        ; --- Empilage de l'adresse de tampon
         LDA     '\n',i      ; --- Empilage du caract�re � ajouter
         STA     -2,s        ; ----
         SUBSP   4,i         ; --- Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; ---
         BR      ENDIF06,i   ; ---
ELSE06:  NOP0                ; -- Sinon on affiche 2 espaces
         LDA     2,i         ; --- Empilage du nombre d'espace � ajouter
         STA     -2,s        ; ----
         LDA     tampon,i    ; --- Empilage de l'addresse du tampon
         STA     -4,s        ; ----
         SUBSP   4,i         ; --- #qteesp5 #addtpn5 
         CALL    AjtEsTmp,i  ; ---
ENDIF06: NOP0                ; ---

         ; # Fin boucle jours ########
         ADDX    1,i
         BR   FTEST02,i
ENDFOR02:ADDSP   4,i         ; - Variables de calcul -> #vartmp02 #vartmp01 

         LDA     tampon,i    ; - Ajouter un saut de ligne
         STA     -4,s        ; - Empilage de l'adresse de tampon
         LDA     '\n',i      ; - Empilage du caract�re � ajouter
         STA     -2,s        ; --
         SUBSP   4,i         ; - Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; -
         LDA     tampon,i    ; - Ajouter un saut de ligne
         STA     -4,s        ; - Empilage de l'adresse de tampon
         LDA     '\n',i      ; - Empilage du caract�re � ajouter
         STA     -2,s        ; --
         SUBSP   4,i         ; - Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; -

         NOP0                ; => Affichage des jours de paie
         LDX     tampon,i    ; -
         STX     -8,s        ; -
         LDX     txtpaies,i  ; -
         STX     -6,s        ; -
         LDX     0,i         ; -
         STX     -4,s        ; -
         LDX     21,i        ; -
         STX     -2,s        ; -
         SUBSP   8,i         ; - Allocation de l'espace sur le stack #qtechrc6 #posdebu6 #addrorg6 #addrtpn6 
         CALL    CpTampon,i  ; -
         
         LDX     1,i
WHILE03: CPX     nbjrspai,i
         BRGT    ENDWHI03,i
         SUBX    1,i         ; - Chargement du jour
         ASLX                ; --
         LDA     jourspai,x  ; --
         ASRX                ; --
         ADDX    1,i         ; --
         STA     -2,s        ; - Empilage du jour
         LDA     tampon,i    ; - Empilage de l'adresse du tampon 
         STA     -4,s        ; --
         SUBSP   4,i         ; - #numero7 #addrtpn7
         CALL    AjtNmTpn,i  ; -
         
         CPX     nbjrspai,d  ; - Si c'est le derni� jour, pas de virgule.
         BREQ    ENDWHI03,i  ; --
         LDA     tampon,i    ; - Ajouter une virgule
         STA     -4,s        ; - Empilage de l'adresse de tampon
         LDA     ',',i       ; - Empilage du caract�re � ajouter
         STA     -2,s        ; --
         SUBSP   4,i         ; - Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; -
         
         CPX     2,i         ; - S'il y a un troisi�me jours on saute la ligne
         BRLT    skipsaut,i  ; --
         LDA     tampon,i    ; - Ajouter un saut de ligne
         STA     -4,s        ; - Empilage de l'adresse de tampon
         LDA     '\n',i       ; - Empilage du caract�re � ajouter
         STA     -2,s        ; --
         SUBSP   4,i         ; - Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; -
         LDA     21,i        ; --- Empilage du nombre d'espace � ajouter
         STA     -2,s        ; ----
         LDA     tampon,i    ; --- Empilage de l'addresse du tampon
         STA     -4,s        ; ----
         SUBSP   4,i         ; --- #qteesp5 #addtpn5 
         CALL    AjtEsTmp,i  ; ---

skipsaut:ADDX    1,i
         BR      WHILE03,i
ENDWHI03:NOP0

         LDA     tampon,i    ; --- Ajouter un saut de ligne
         STA     -4,s        ; --- Empilage de l'adresse de tampon
         LDA     '\n',i      ; --- Empilage du caract�re � ajouter
         STA     -2,s        ; ----
         SUBSP   4,i         ; --- Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; ---

         LDA     tampon,i    ; --- Ajouter un saut de ligne
         STA     -4,s        ; --- Empilage de l'adresse de tampon
         LDA     '\n',i      ; --- Empilage du caract�re � ajouter
         STA     -2,s        ; ----
         SUBSP   4,i         ; --- Allocation de l'espace sur le stack #caract4 #addrtpn4
         CALL    AjtRpTmp,i  ; ---

         ; Affichage final
         LDX     curseur,d
         LDA     0,i
         STBYTEA tampon,x
         STRO    tampon,d

         BR      debut,i

invalidc:STRO    msginvld,d
         BR      debut,i
fin:     STOP


; ===============================================================================================================================================================
;                                                                          --- DONN�ES ---
; ===============================================================================================================================================================
chaine:  .BLOCK  7           ; #1h7a
annee:   .BLOCK  2           ; #2d
mois:    .BLOCK  2           ; #2d 
jours:   .BLOCK  2           ; #2d Jours absolus du mois
jourdebu:.BLOCK  2           ; #2d Jour du d�but du mois
nbjrspai:.BLOCK  2           ; #2d Nombre de jours de paie
jourspai:.WORD   0           ; Jour de paie 1
         .WORD   0           ; Jour de paie 2
         .WORD   0           ; Jour de paie 3
qtejmois:.BLOCK  2           ; #2d Nombre de jours dans le mois
bisextil:.BLOCK  2           ; #2d Si l'ann�e est bissextile
tbljours:.WORD   31          ; Janvier
         .WORD   28          ; F�vrier
         .WORD   31          ; Mars
         .WORD   30          ; Avril
         .WORD   31          ; Mai
         .WORD   30          ; Juin
         .WORD   31          ; Juillet
         .WORD   31          ; Ao�t
         .WORD   30          ; Septembre
         .WORD   31          ; Octobre
         .WORD   30          ; Novembre
         .WORD   31          ; D�cembre
txtpaies:.ASCII  "      Jours de paie: \x00"
txtidjrs:.ASCII  "       D  L  M  M  J  V  S\n\n\x00"
txtmois: .ASCII  "Janvier-F�vrier-Mars-Avril-Mai-Juin-Juillet-Ao�t-Septembre-Octobre-Novembre-D�cembre-\x00"
tblmois: .WORD   0           ; D�but Janvier - Table d'index de la liste des mois
         .WORD   8           ; D�but F�vrier
         .WORD   16          ; D�but Mars
         .WORD   21          ; D�but Avril
         .WORD   27          ; D�but Mai
         .WORD   31          ; D�but Juin
         .WORD   36          ; D�but Juillet
         .WORD   44          ; D�but Ao�t
         .WORD   49          ; D�but Septembre
         .WORD   59          ; D�but Octobre
         .WORD   67          ; D�but Novembre
         .WORD   76          ; D�but D�cembre
msgintro:.ASCII  "Bievenue dans le programme calend.\n"
         .ASCII  "Ce programme sert � afficher un\n"
         .ASCII  "mois dans une ann�e et calculer les \n"
         .ASCII  "jours de paie.\n\n"
         .ASCII  "Pour utiliser ce programme, entrez une\n"
         .ASCII  "ann�e et un mois au format AAAAMM sans\n"
         .ASCII  "espace avant ou apr�s la valeur:\n"
         .ASCII  "Ex: 199704 Pour Avril 1997\n\n"
         .ASCII  "Pour quitter, entrez: 9999\n\n\x00"
msgquest:.ASCII  "Entrez la date d�sir�e: \x00"
msginvld:.ASCII  "Entr�e invalide \n\x00"
vartmp01:.EQUATE 0           ; #2h Variable temporaire 1
vartmp02:.EQUATE 2           ; #2h Variable temporaire 2
curseur: .BLOCK  2           ; #2h Curseur du tampon
tampon:  .BLOCK  254
         .BYTE   0
; ===============================================================================================================================================================
;                                                                 --- SOUS-PROGRAMME: Multiplier ---
; ===============================================================================================================================================================
sauvea1: .EQUATE 0           ; #2h Variable locale -> Sauvegarde du registre A
sauvex1: .EQUATE 2           ; #2h Variable locale -> Sauvegarde du registre X
retAddr1:.EQUATE 4           ; #2h Addresse de retour
facteur1:.EQUATE 6           ; #2h Op�rande 1
facteur2:.EQUATE 8           ; #2h Op�rande 2
produit: .EQUATE 10          ; #2h Variable de retour -> Produit des deux facteurs

multipli:SUBSP   4,i         ; #sauvea1, #sauvex1
         STA     sauvea1,s
         STX     sauvex1,s
         LDA     0,i
         LDX     facteur2,s
         STA     produit,s
         LDA     produit,s
bouclemr:CPX     0,i
         BRLE    finmulti,i
         ADDA    facteur1,s
         SUBX    1,i
         BR      bouclemr,i
finmulti:STA     produit,s
         LDA     retAddr1,s   ; Restoration de l'addresse de retour
         STA     facteur2,s   ; -
         LDA     sauvea1,s    ; Restoration du registre A
         LDX     sauvex1,s    ; Restoration du registre X
         ADDSP   8,i         ; #facteur1 #retAddr1 #sauvex1, #sauvea1 
         RET0


; ===============================================================================================================================================================
;                                                                 --- SOUS-PROGRAMME: Diviser ---
; ===============================================================================================================================================================
sauvea2: .EQUATE 0           ; #2h Variable locale -> Sauvegarde du registre A
sauvex2: .EQUATE 2           ; #2h Variable locale -> Sauvegarde du registre X
retAddr2:.EQUATE 4           ; #2h Addresse de retour
dividend:.EQUATE 6           ; #2h Op�rande 1
diviseur:.EQUATE 8           ; #2h Op�rande 2
quotient:.EQUATE 10          ; #2h Variable de retour -> Quotient
reste:   .EQUATE 12          ; #2h Variable de retour -> Reste

diviser: NOP0 
         SUBSP   4,i         ; #sauvea2, #sauvex2
         STA     sauvea2,s
         STX     sauvex2,s
         LDA     0,i
         LDX     0,i
         STA     quotient,s
         STA     reste,s
         LDA     dividend,s

boucledr:ADDX    1,i
         SUBA    diviseur,s
         BRGE    boucledr,i
         SUBX    1,i
         ADDA    diviseur,s

         STA     reste,s     ; Registre A est le reste de la division
         STX     quotient,s  ; - Enregistrement du quotient
         LDA     retAddr2,s  ; Restoration de l'addresse de retour
         STA     diviseur,s  ; -
         LDA     sauvea2,s   ; Restoration du registre A
         LDX     sauvex2,s   ; Restoration du registre X
         ADDSP   8,i         ; #dividend #retAddr2 #sauvex2, #sauvea2
         RET0


; ===============================================================================================================================================================
;                                                                 --- SOUS-PROGRAMME: ASCII vers i16 ---
;param�tres:     Addresse du premier num�ro ascii
;                Nombre de caract�re dans le chiffre
;retour:         Chiffre en integer 16 bits
; ===============================================================================================================================================================
sauvea3: .EQUATE 0           ; #2h Variable locale -> Sauvegarde du registre A 
sauvex3: .EQUATE 2           ; #2h Variable locale -> Sauvegarde du registre X
nbtemp:  .EQUATE 4           ; #2h Variable locale -> Chiffre temporaire
retAddr: .EQUATE 6           ; #2h Addresse de retour
addrprot:.EQUATE 8           ; #2h Param�tre 1 -> Addresse du premier num�ro ascii
nbcaract:.EQUATE 10          ; #2h Param�tre 2 -> Nombre de caract�re dans le chiffre
nbeni16: .EQUATE 12          ; #2h Variable de retour -> Chiffre en integer 16 bits

AsciiI16:NOP0
         SUBSP   6,i         ; #nbtemp #sauvex3, #sauvea3 
         STA     sauvea3,s    ; Sauvegarde du registre A
         STX     sauvex3,s    ; Sauvegarde du registre X
         LDX     0,i
         LDA     0,i
         STA     nbtemp,s    ; Initialisation de la variable locale #nbtemp
         STA     nbeni16,s   ; Initialisation de la variable locale #nbeni16
boucletr:LDA     nbeni16,s   ; D�but de la boucle de transformation des chiffres ASCII en nombre i16
         STA     nbtemp,s
         ASLA                ; *2
         ASLA                ; *4
         ADDA    nbtemp,s    ; *5
         ASLA                ; *10
         STA     nbtemp,s   
         LDA     0,i          ; R�initialisation du registre A, pour charger un seul octet
         LDBYTEA addrprot,sxf ; Chargement du caract�re de la chaine
         SUBA    '0',i
         ADDA    nbtemp,s
         STA     nbeni16,s   ; Sauvegarde du nombre apr�s une it�ration
         LDA     nbcaract,s  ; D�cr�mentation du numbre de caract�res restants
         SUBA    1,i         ; -
         STA     nbcaract,s  ; -
         ADDX    1,i         ; Incr�mentation de l'index
         CPA     0,i         
         BRGT    boucletr,i  ; Fin de la boucletr
         LDA     retAddr,s   ; Restoration de l'addresse de retour
         STA     nbcaract,s  ; -
         LDA     sauvea3,s    ; Restoration du registre A
         LDX     sauvex3,s    ; Restoration du registre X
         ADDSP   10,i        ; #addrprot #retAddr #nbtemp #sauvex3, #sauvea3
         RET0

; ===============================================================================================================================================================
;                                                             --- SOUS-PROGRAMME: Ajouts rapide au tampon ---
;
; Ce sous-programme sert � rajouter rapidement un caract�re (A) au tampon (X). Il cherche la fin du contenu dans le tampon et rajoute le caract�re.
;param�tres:     Registre A -> Octe � ajouter � la fin du texte dans le tampon
;                Registre X -> Addresse du tampon
;retour          Rien
; ===============================================================================================================================================================
sauvea4: .EQUATE 0           ; #2h Variable locale -> Sauvegarde du registre A 
sauvex4: .EQUATE 2           ; #2h Variable locale -> Sauvegarde du registre X
addrcpt4:.EQUATE 4           ; #2h Variable locale -> Adresse du curseur du tampon
retAddr4:.EQUATE 6           ; #2h Adresse de retour
addrtpn4:.EQUATE 8           ; #2h Param�tre 1 -> Addresse d'�criture dans le tampon
caract4: .EQUATE 10          ; #2h Param�tre 2 -> Caracter a ajouter au tampon

AjtRpTmp:NOP0
         SUBSP   6,i         ; #addrcpt4 #sauvex4 #sauvea4
         STA     sauvea4,s
         STX     sauvex4,s

         LDX     addrtpn4,s   ; Charger l'adresse du compteur du tampon
         SUBX    2,i         ; -
         STX     addrcpt4,s  ; -

         LDX     0,i
         LDA     0,i

         NOP0                ; Trouver un octet libre dans le tampon
         LDX     addrcpt4,sf ; - On charge le contenu du curseur
         ADDX    addrtpn4,s  ; - On y ajoute l'adresse du tampon
         STX     addrtpn4,s  ; - On enregistre (adresse tampon + contenu curseur)

         LDA     caract4,s   ; -
         STBYTEA addrtpn4,sf ; -

         LDX     addrcpt5,sf ; On incr�mente le curseur du nombre de caract�res ajout�s
         ADDX    1,i         ;--
         STX     addrcpt5,sf ; --

         LDA     retAddr4,s  ; Restauration de l'adresse de retour
         STA     caract4,s   ; -
         LDA     sauvea4,s   ; Restauration des registres et nettoyage
         LDX     sauvex4,s   ; -
         ADDSP   10,i        ; - #addrtpn4 #retAddr4 #addrcpt4 #sauvex4 #sauvea4
         RET0


; ===============================================================================================================================================================
;                                                           --- SOUS-PROGRAMME: Ajouts d'espace au tampom ---
;
; Ce sous-programme sert � rapidement ajouter une quanti� d'espaces (A) au tampon (X). Il cherche la fin du contenu dans le tampon et y ajoute les espaces.
;param�tres:     Registre A -> Quantit� d'espaces � ajouter
;                Registre X -> Addresse du tampon
;retour          Rien
; ===============================================================================================================================================================
sauvea5: .EQUATE 0           ; #2h Variable locale -> Sauvegarde du registre A 
sauvex5: .EQUATE 2           ; #2h Variable locale -> Sauvegarde du registre X
addrcpt5:.EQUATE 4           ; #2h Variable locale -> Adresse du curseur du tampon
retAddr5:.EQUATE 6           ; #2h Adresse de retour
addtpn5: .EQUATE 8           ; #2h Param�tre 1 -> Addresse du tampon
qteesp5: .EQUATE 10          ; #2h Param�tre 2 -> Quantit� d'espaces � ajouter

AjtEsTmp:NOP0
         SUBSP   6,i         ; #addrcpt5 #sauvex5 #sauvea5 
         STA     sauvea5,s
         STX     sauvex5,s

         LDX     addtpn5,s   ; Charger l'adresse du compteur du tampon
         SUBX    2,i         ; -
         STX     addrcpt5,s  ; -

         LDX     0,i
         LDA     0,i

         NOP0                ; Trouver un octet libre dans le tampon
         LDX     addrcpt5,sf ; - On charge le contenu du curseur
         ADDX    addtpn5,s   ; - On y ajoute l'adresse du tampon
         STX     addtpn5,s   ; - On enregistre (adresse tampon + contenu curseur)
         
FOR50:   LDX     0,i         ; �criture des espaces (Initialisation de la boucle)
         LDA     ' ',i       ; -
FTEST50: CPX     qteesp5,s   ; - Quand X est �gal � la quantit� d'espace on a termin�
         BRGE    ENDFOR50,i  ; -
         STBYTEA addtpn5,sxf
         ADDX    1,i         
         BR      FTEST50,i 
ENDFOR50:NOP0

         ADDX    addrcpt5,sf ; On incr�mente le curseur du nombre de caract�res ajout�s
         STX     addrcpt5,sf ; --

         LDA     retAddr5,s  ; Restauration de l'adresse de retour
         STA     qteesp5,s   ; -
         LDA     sauvea5,s   ; Restauration des registre et nettoyage
         LDX     sauvex5,s   ; -
         ADDSP   10,i         ; - #addtpn5 #retAddr5 #addrcpt5 #sauvex5 #sauvea5 
         RET0

; ===============================================================================================================================================================
;                                                           --- SOUS-PROGRAMME: Copier dans le tampon ---
;
; Ce sous-programme sert � copier une une chaine � la fin du tampon. Il cherche la fin du contenu dans le tampon et y ajoute la chaine.
;param�tres:     Addresse du tampon
;                Addresse de la chaine d'origine
;                Position relative au d�but de la chaine d'origine
;                Quantit� de caract�res � copier
;retour          Rien
; ===============================================================================================================================================================
sauvea6: .EQUATE 0           ; #2h Variable locale -> Sauvegarde du registre A 
sauvex6: .EQUATE 2           ; #2h Variable locale -> Sauvegarde du registre X
addrcpt6:.EQUATE 4           ; #2h Variable locale -> Adresse du curseur du tampon
retAddr6:.EQUATE 6           ; #2h Addresse de retour
addrtpn6:.EQUATE 8           ; #2h Param�tre 1 -> Addresse du tampon
addrorg6:.EQUATE 10          ; #2h Param�tre 2 -> Addresse de la chaine d'orgine
posdebu6:.EQUATE 12          ; #2h Param�tre 3 -> Position relative au d�but de la chaine d'origine
qtechrc6:.EQUATE 14          ; #2h Param�tre 4 -> Quantit� de caract�res copi�s

CpTampon:NOP0
         SUBSP   6,i         ; #addrcpt6 #sauvex6 #sauvea6
         STA     sauvea6,s
         STX     sauvex6,s

         LDX     addrtpn6,s  ; Charger l'adresse du curseur du tampon 
         SUBX    2,i         ; -
         STX     addrcpt6,s  ; -

         LDA     addrorg6,s  ; On ajoute la position relative au pointeur de la chaine copi�
         ADDA    posdebu6,s  ; -
         STA     addrorg6,s  ; -

         LDA     0,i         ; Initialisation des registres
         LDX     0,i         ; -

         NOP0                ; Chercher un octet libre dans le tampon
         LDX     addrcpt6,sf ; - On charge le contenu du curseur
         ADDX    addrtpn6,s  ; - On y ajoute l'adresse du tampon
         STX     addrtpn6,s  ; - On enregistre (adresse tampon + contenu curseur)
         
FOR60:   LDX     0,i         ; �criture de la copie (Initialisation de la boucle)
         LDA     0,i         ; -
FTEST60: CPX     qtechrc6,s  ; - Quand X est �gal a la position finale on arr�te 
         BRGE    ENDFOR60,i  ; -
         LDBYTEA addrorg6,sxf; -
         STBYTEA addrtpn6,sxf; -
         ADDX    1,i         ; -
         BR      FTEST60,i   ; -
ENDFOR60:NOP0                

         ADDX    addrcpt6,sf ; On incr�mente le curseur du nombre de caract�res ajout�s
         STX     addrcpt6,sf ; --

         LDA     sauvea6,s   ; Restauration des registres et nettoyage
         LDX     sauvex6,s   ; -
         LDA     retAddr6,s
         STA     qtechrc6,s
         ADDSP   14,i        ; - #posdebu6 #addrorg6 #addrtpn6 #retAddr6 #addrcpt5 #sauvex6 #sauvea6 
         RET0

; ===============================================================================================================================================================
;                                                            --- SOUS-PROGRAMME: Ajouts num�ro au tampon ---
;
; Ce sous-programme sert � convertir un num�ro en chiffres et � l'ajouter au tampon. Il cherche la fin du contenu dans le tampon et y ajoute les chiffres.
;param�tres:     Addresse du tampon
;                Num�ro � ajouter au tampon
;retour          Rien
; ===============================================================================================================================================================
sauvea7: .EQUATE 0           ; #2h Variable locale -> Sauvegarde du registre A 
sauvex7: .EQUATE 2           ; #2h Variable locale -> Sauvegarde du registre X
carac47: .EQUATE 4           ; #1c Variable locale -> Caract�re 1
carac37: .EQUATE 5           ; #1c Variable locale -> Caract�re 1
carac27: .EQUATE 6           ; #1c Variable locale -> Caract�re 1
carac17: .EQUATE 7           ; #1c Variable locale -> Caract�re 1
qtecars7:.EQUATE 8           ; #2h Variable locale -> Nombre de caract�res
addrcpt7:.EQUATE 10          ; #2h Variable locale -> Adresse du curseur du tampon
retAddr7:.EQUATE 12          ; #2h Addresse de retour
addrtpn7:.EQUATE 14          ; #2h Param�tre 1 -> Addresse d'�criture dans le tampon
numero7: .EQUATE 16          ; #2h Param�tre 2 -> Num�ro � ajouter au tampon

AjtNmTpn:NOP0
         SUBSP   12,i        ; #addrcpt6 #qtecars7 #carac17 #carac27 #carac37 #carac47 #sauvex4 #sauvea4 
         STA     sauvea4,s
         STX     sauvex4,s

         LDX     addrtpn7,s  ; Charger l'adresse du curseur du tampon 
         SUBX    2,i         ; -
         STX     addrcpt7,s  ; -

         LDX     0,i
         STBYTEX carac17,s
         STBYTEX carac27,s
         STBYTEX carac37,s
         STBYTEX carac47,s

         LDA     numero7,s   ; Convertir le num�ro i16 en ASCII
WHILE70: NOP0                ; -
         CPA     0,i         ; -
         BRLE    ENDWHI70,i  ; -
         CPX     4,i         ; - Si plus de 4 chiffres on arr�te, car on a pas de place pour en garder plus
         BREQ    ENDWHI70,i  ; --
         STA     -8,s        ; -
         LDA     10,i        ; -
         STA     -6,s        ; -
         SUBSP   8,i         ; - Allocation de l'espace sur le stack #reste #quotient #diviseur #dividend
         CALL    diviser,i   ; -
         ADDSP   4,i         ; - #reste #quotient
         LDA     -2,s        ; - R�cup�rer le reste
         ADDA    '0',i       ; -- Convertir en caract�re
         STBYTEA carac47,sx  ; -- Enregistrer
         LDA     -4,s        ; - R�cup�rer le quotient
         ADDX    1,i         ; - Incr�menter le compteur
         BR      WHILE70,i   ; -
ENDWHI70:SUBX    1,i         ; - On enl�ve un car on commence � z�ro
         STX     qtecars7,s  ; -

         NOP0                ; Chercher un octet libre dans le tampon
         LDX     addrcpt7,sf ; - On charge le contenu du curseur
         ADDX    addrtpn7,s  ; - On y ajoute l'adresse du tampon
         STX     addrtpn7,s  ; - On enregistre (adresse tampon + contenu curseur)
         
         LDA     0,i         ; �crire les chiffres dans le tampon
         LDX     0,i         ; -
WHILE72: NOP0                ; - 
         CPX     qtecars7,s  ; -
         BRGT    ENDWHI72,i  ; -
         CPX     0,i         ; -
         BRLT    ENDWHI72,i  ; -
         NOP0                ; - Position inverse = Compl�ment(X - qtecars7) 
         SUBX    qtecars7,s  ; --
         NOTX                ; --
         ADDX    1,i         ; --
         LDBYTEA carac47,sx  ; -- Lecture du caract�re invers�
         NOTX                ; -- Op�ration inverse Compl�ment(Compl�ment(X - qtecars7)) + qtecars7
         ADDX    1,i         ; ---
         ADDX    qtecars7,s  ; ---
ecrirec7:STBYTEA addrtpn7,sxf; - �criture 
         ADDX    1,i         ; -
         BR      WHILE72,i   ; -
ENDWHI72:NOP0

         ADDX    addrcpt7,sf ; On incr�mente le curseur du nombre de caract�res ajout�s
         STX     addrcpt7,sf ; --

         LDA     retAddr7,s
         STA     numero7,s
         LDA     sauvea7,s
         LDX     sauvex7,s 
         ADDSP   16,i        ; #addrtpn7 #retAddr7 #addrcpt7 #qtecars7 #carac17 #carac27 #carac37 #carac47 #sauvex4 #sauvea4 
         RET0

         .END

